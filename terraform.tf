terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "> 1.9.0"
    }
    local = {
      source = "hashicorp/local"
    }
    null = {
      source = "hashicorp/null"
    }
  }

  required_version = ">= 0.13.0"
}

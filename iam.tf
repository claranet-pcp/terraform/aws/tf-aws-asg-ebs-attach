data "aws_iam_policy_document" "lambda_policy" {
  # Lambda policy for attaching EBS
  statement {
    actions = [
      "ec2:AttachVolume",
      "ec2:DescribeVolumeAttribute",
      "ec2:DescribeVolumeStatus",
      "ec2:DescribeVolumes",
      "ec2:DescribeVolumesModifications",
      "ec2:DescribeInstances",
      "autoscaling:CompleteLifecycleAction",
      "autoscaling:Describe*",
    ]

    resources = [
      "*",
    ]
  }

  # Lambda policy for running SSM command
  dynamic "statement" {
    for_each = var.enable_ssm ? [var.enable_ssm] : []
    content {
      actions = [
        "ssm:SendCommand",
      ]

      resources = [
        "arn:aws:ec2:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:instance/*",
        "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:document/${aws_ssm_document.ssm[0].name}",
      ]
    }
  }

  dynamic "statement" {
    for_each = var.enable_ssm ? [var.enable_ssm] : []
    content {
      actions = [
        "ssm:GetCommandInvocation",
        "ssm:DescribeInstanceInformation",
      ]

      resources = [
        "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*",
      ]
    }
  }
}

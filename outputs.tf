output "lambda_function_name" {
  value = join("", module.lambda.*.function_name)
}

output "lambda_function_arn" {
  value = join("", module.lambda.*.function_arn)
}

output "lambda_role_arn" {
  value = join("", module.lambda.*.role_arn)
}

output "lambda_role_name" {
  value = join("", module.lambda.*.role_name)
}
